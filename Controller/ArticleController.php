<?php

namespace ATM\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\ArticleBundle\Form\ArticleType;
use ATM\ArticleBundle\Entity\Article;
use ATM\ArticleBundle\Entity\Category;

class ArticleController extends Controller
{
    public function indexAction(){
        return $this->render('ATMArticleBundle:Common:index.html.twig');
    }

    public function showArticlesAction(){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('a')
            ->addSelect('c')
            ->from('ATMArticleBundle:Article','a')
            ->join('a.category','c')
            ->orderBy('a.publishDate','DESC');

        return $this->render('ATMArticleBundle:Article:articlesOverview.html.twig',array(
            'articles' => $qb->getQuery()->getArrayResult()
        ));
    }

    public function createArticleAction(){
        $request = $this->get('request_stack')->getCurrentRequest();
        $config = $this->getParameter('atm_article_config');

        $article = new Article();
        $form = $this->createForm(ArticleType::class,$article,array(
            'categoryNamespace' => Category::class
        ));
        $errors = array();
        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $user = $this->get('security.token_storage')->getToken()->getUser();

                $article->setAuthor($user );

                $file = $request->files->get('flThumbnail');
                $filename = md5(uniqid()) . '.' . $file->guessExtension();

                $imagePath = $this->get('kernel')->getRootDir() . '/../web/' . $config['media_folder'];
                if (!is_dir($imagePath)) {
                    mkdir($imagePath);
                }

                $file->move($imagePath, $filename);


                $article->setThumbnail($config['media_folder'] . '/' . $filename);
                $em->persist($article);
                $em->flush();

                return new RedirectResponse($this->get('router')->generate('atm_article_show_articles'));
            }
        }


        return $this->render('ATMArticleBundle:Article:createArticle.html.twig',array(
            'form' => $form->createView(),
            'useCategories' => $config['use_categories'],
            'errors' => $errors
        ));
    }

    public function editArticleAction($articleId){
        $request = $this->get('request_stack')->getCurrentRequest();
        $config = $this->getParameter('atm_article_config');
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('ATMArticleBundle:Article')->findOneById($articleId);
        $form = $this->createForm(ArticleType::class,$article,array(
            'categoryNamespace' => Category::class
        ));

        $errors = array();
        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $user = $this->get('security.token_storage')->getToken()->getUser();

                $article->setAuthor($user);

                $file = $request->files->get('flThumbnail');
                if (!is_null($file)) {

                    if($article->getThumbnail()){
                        unlink($this->get('kernel')->getRootDir() . '/../web/' . $article->getThumbnail());
                    }


                    $filename = md5(uniqid()) . '.' . $file->guessExtension();
                    $imagePath = $this->get('kernel')->getRootDir() . '/../web/' . $config['media_folder'];
                    if (!is_dir($imagePath)) {
                        mkdir($imagePath);
                    }
                    $file->move($imagePath, $filename);
                    $article->setThumbnail($config['media_folder'] . '/' . $filename);

                }
                $em->persist($article);
                $em->flush();

                return new RedirectResponse($this->get('router')->generate('atm_article_show_articles'));
            }
        }


        return $this->render('ATMArticleBundle:Article:updateArticle.html.twig',array(
            'article' => $article,
            'errors' => $errors,
            'useCategories' => $config['use_categories'],
            'form' => $form->createView()
        ));
    }

    public function deleteArticleAction($articleId){
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('ATMArticleBundle:Article')->findOneById($articleId);

        if(file_exists($this->get('kernel')->getRootDir() . '/../web/'.$article->getThumbnail())){
            unlink($this->get('kernel')->getRootDir() . '/../web/'.$article->getThumbnail());
        }
        $em->remove($article);
        $em->flush();

        return new RedirectResponse($this->get('router')->generate('atm_article_show_articles'));
    }
}
