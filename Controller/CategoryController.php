<?php

namespace ATM\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\ArticleBundle\Form\CategoryType;
use ATM\ArticleBundle\Entity\Category;

class CategoryController extends Controller
{
    public function createCategoryAction(){
        $request = $this->get('request_stack')->getCurrentRequest();

        $category = new Category();
        $form = $this->createForm(CategoryType::class,$category);
        $errors = array();
        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();

                $file = $request->files->get('flThumbnail');
                $filename = md5(uniqid()) . '.' . $file->guessExtension();
                $config = $this->getParameter('atm_article_config');
                $imagePath = $this->get('kernel')->getRootDir() . '/../web/' . $config['media_folder'];
                if (!is_dir($imagePath)) {
                    mkdir($imagePath);
                }

                $file->move($imagePath, $filename);

                $category->setThumbnail($config['media_folder'] . '/' . $filename);

                $em->persist($category);
                $em->flush();

                return new RedirectResponse($this->get('router')->generate('atm_article_show_categories'));
            }
        }

        return $this->render('ATMArticleBundle:Category:createCategory.html.twig',array(
            'form' => $form->createView(),
            'errors' => $errors
        ));
    }

    public function showCategoriesAction(){
        $categories = $this->get('atm_article_category_crud')->getCategories();

        return $this->render('ATMArticleBundle:Category:categoriesOverview.html.twig',array(
            'categories' => $categories
        ));
    }

    public function editCategoryAction($categoryId){
        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('ATMArticleBundle:Category')->findOneById($categoryId);

        $form = $this->createForm(CategoryType::class,$category);
        $errors = array();
        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isValid()){

                $file = $request->files->get('flThumbnail');
                if($file){
                    if($category->getThumbnail()){
                        unlink($this->get('kernel')->getRootDir() . '/../web/'.$category->getThumbnail());
                    }
                    $filename = md5(uniqid()) . '.' . $file->guessExtension();
                    $config = $this->getParameter('atm_article_config');
                    $imagePath = $this->get('kernel')->getRootDir() . '/../web/' . $config['media_folder'];
                    if (!is_dir($imagePath)) {
                        mkdir($imagePath);
                    }

                    $file->move($imagePath, $filename);
                    $category->setThumbnail($config['media_folder'] . '/' . $filename);
                }

                $em->persist($category);
                $em->flush();
                return new RedirectResponse($this->get('router')->generate('atm_article_show_categories'));
            }
        }

        return $this->render('ATMArticleBundle:Category:updateCategory.html.twig',array(
            'form' => $form->createView(),
            'category' => $category,
            'errors' => $errors
        ));
    }

    public function deleteCategoryAction($categoryId){
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('ATMArticleBundle:Category')->findOneById($categoryId);
        unlink($this->get('kernel')->getRootDir() . '/../web/'.$category->getThumbnail());
        $em->remove($category);
        $em->flush();
        return new RedirectResponse($this->get('router')->generate('atm_article_show_categories'));

    }
}
