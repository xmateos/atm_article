<?php

namespace ATM\ArticleBundle\Services;



class CategoryCRUD
{
    private $em;
    private $config;

    public function __construct($em,$config){
        $this->em = $em;
        $this->config = $config;
    }

    public function persistCategory($form,$category){

        if($form->isValid()){
            try{
                $this->em->persist($category);
                $this->em->flush();
            }catch(\Exception $e){
                return array($e->getMessage());
            }
        }else{
            return $this->getErrorMessages($form);
        }

        return true;
    }

    public function getCategories(){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('c')
            ->from('ATMArticleBundle:Category','c')
            ->orderBy('c.name');

        return $qb->getQuery()->getArrayResult();
    }

    public function deleteCategory($category){
        try{
            $this->em->remove($category);
            $this->em->flush();
            return true;
        }catch(\Exception $e){
            return array($e->getMessage());
        }
    }

    private function getErrorMessages($form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}