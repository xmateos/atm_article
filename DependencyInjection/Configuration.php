<?php

namespace ATM\ArticleBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;


class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder->root('atm_article')
            ->children()
                ->arrayNode('class')->isRequired()
                    ->children()
                        ->arrayNode('model')->isRequired()
                            ->children()
                                ->scalarNode('user')->isRequired()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->scalarNode('use_categories')->defaultValue(false)->end()
                ->scalarNode('media_folder')->end()
            ->end();


        return $treeBuilder;
    }
}
