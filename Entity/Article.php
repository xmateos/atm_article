<?php

namespace ATM\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ATM\ArticleBundle\Helpers\Functions;
use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ATM\ArticleBundle\Repository\ArticleRepository")
 * @ORM\Table(name="atm_article")
 */
class Article
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
    */
    protected $title;

    /**
     * @ORM\Column(name="title_canonical", type="string", length=255, nullable=false)
     */
    protected $titleCanonical;

    /**
     * @ORM\Column(name="summary", type="string", length=255, nullable=true)
     */
    protected $summary;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    protected $creationDate;

    /**
     * @ORM\Column(name="publish_date", type="datetime", nullable=false)
     */
    protected $publishDate;

    /**
     * @ORM\Column(name="body", type="text", nullable=false)
     */
    protected $body;

    /**
     * @ORM\Column(name="thumbnail", type="text", nullable=false)
     */
    protected $thumbnail;

    protected $author;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="articles")
     */
    protected $category;


    public function __construct()
    {
        $this->creationDate = new DateTime();
        $this->publishDate = new DateTime();
    }


    public function getId()
    {
        return $this->id;
    }

    public function getSummary()
    {
        return $this->summary;
    }

    public function setSummary($summary)
    {
        $this->summary = $summary;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    public function getPublishDate()
    {
        return $this->publishDate;
    }

    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getTitleCanonical()
    {
        return $this->titleCanonical;
    }

    public function setTitleCanonical($title)
    {
        $functions = new Functions();
        $titleCanonical = $functions->canonicalizeString($title);
        $this->titleCanonical = $titleCanonical;
    }

    public function setTitle($title){
        $this->title = $title;
        $this->setTitleCanonical($title);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getThumbnail()
    {
        return $this->thumbnail;
    }
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }
}